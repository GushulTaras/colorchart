package taras.gushul.com.piechart.chart;

import android.content.Context;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.util.ArrayList;
import java.util.List;

import taras.gushul.com.piechart.R;

/**
 * Created: Gushul Taras
 * E-mail: gushultaras125@gmail.com
 * Company: APPGRANULA LLC
 * Date: 07.09.2018
 */
public class ColorChart extends FrameLayout {

    private static final float MAX_FACTOR = 1.05f;
    private static int X_AXIS_RENDERER_LIMIT = 12;
    private LineChart chart;

    private List<Float> values;
    private List<String> dates;
    private int[] colors;
    private float positions[];
    private float maxBorder;
    private float minBorder;
    private Float leftOffsetValue;
    private Float rightOffsetValue;

    public ColorChart(@NonNull Context context) {
        super(context);
        init();
    }

    public ColorChart(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ColorChart(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ColorChart(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        View view = inflate(getContext(), R.layout.widget_color_chart, null);
        addView(view);
        chart = findViewById(R.id.lineChart);
    }

    public void setData(float max,
                        float min,
                        @Nullable Float leftOffsetValue,
                        @Nullable Float rightOffsetValue,
                        @NonNull List<Float> values,
                        @NonNull List<String> dates,
                        @NonNull int[] colors,
                        @NonNull float positions[]) {
        if (values.size() != dates.size()) {
            throw new IllegalArgumentException("values and dates arrays must be of equal length");
        }
        if (colors.length < 2) {
            throw new IllegalArgumentException("needs >= 2 number of colors");
        }

        this.positions = new float[positions.length];
        this.positions[0] = 0f;
        this.positions[positions.length - 1] = 1f;
        for (int i = 1; i < positions.length - 1; i++) {
            this.positions[i] = positions[i] / positions[positions.length - 1];
        }
        if (leftOffsetValue != null) {
            this.leftOffsetValue = leftOffsetValue;
            values.add(0, leftOffsetValue);
            dates.add(0, "");
            X_AXIS_RENDERER_LIMIT++;
        }
        if (rightOffsetValue != null) {
            this.rightOffsetValue = rightOffsetValue;
            values.add(rightOffsetValue);
            dates.add("");
            X_AXIS_RENDERER_LIMIT++;
        }
        this.values = values;
        this.dates = dates;
        this.colors = colors;

        maxBorder = max * MAX_FACTOR;
        minBorder = min - maxBorder / 20;

        createLineChart();

        postGradient();
    }

    private void createLineChart() {
        chart.setRenderer(new CustomColorLineChartRenderer(chart, chart.getAnimator(), chart.getViewPortHandler()));
        setLineData();
        handleAxis();
        hideDescription();
        if (values.size() <= X_AXIS_RENDERER_LIMIT) {
            chart.setXAxisRenderer(new CustomXAxisRendererChart(chart.getViewPortHandler(), chart.getXAxis(),
                    chart.getTransformer(YAxis.AxisDependency.LEFT), values.size()));
        }
        chart.setTouchEnabled(false);
    }

    private void setLineData() {
        List<Entry> entries = setYAxisValues();
        LineDataSet lineDataSet = new LineDataSet(entries, "");

        lineDataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        lineDataSet.setLineWidth(2f);
        lineDataSet.setDrawValues(false);
        lineDataSet.setDrawCircles(false);

        List<Entry> dots = setYAxisDots();
        LineDataSet dotsDataSet = new LineDataSet(dots, "");
        dotsDataSet.setDrawValues(false);
        dotsDataSet.setCircleRadius(4f);
        dotsDataSet.setDrawCircleHole(false);
        dotsDataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);

        LineData data = new LineData(lineDataSet, dotsDataSet);

        chart.setData(data);
    }

    private List<Entry> setYAxisValues() {
        List<Entry> yVals = new ArrayList<>();

        for (int i = 0; i < values.size(); i++) {
            yVals.add(new Entry(i, values.get(i)));
        }

        return yVals;
    }

    private void handleAxis() {
        chart.getAxisLeft().setDrawLabels(false);
        chart.getAxisLeft().setDrawGridLines(false);
        chart.getAxisLeft().setDrawAxisLine(false);
        chart.getAxisLeft().setAxisMinimum(minBorder);
        chart.getAxisLeft().setAxisMaximum(maxBorder);

        chart.getAxisRight().setDrawLabels(false);
        chart.getAxisRight().setDrawGridLines(false);
        chart.getAxisRight().setDrawAxisLine(false);

        chart.getXAxis().setDrawAxisLine(false);
        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.getXAxis().setValueFormatter(new CustomColorValueFormatter(dates));
        chart.getXAxis().setTextColor(Color.GRAY);

        chart.getXAxis().setGranularityEnabled(true);
    }

    private void hideDescription() {
        Description description = new Description();
        description.setText("");
        chart.setDescription(description);
        chart.getLegend().setEnabled(false);
    }

    private void postGradient() {
        chart.post(new Runnable() {
            @Override
            public void run() {
                addOffset();
                addVerticalGradient();
                addGradient();
                chart.animateY(2000);
                chart.invalidate();
            }
        });
    }

    private void addOffset() {
        if (leftOffsetValue == null && rightOffsetValue == null) {
            return;
        }
        ViewPortHandler viewPortHandler = chart.getViewPortHandler();
        float offset = (0 - chart.getWidth() / values.size()) / 2;
        float left = leftOffsetValue == null ? viewPortHandler.offsetLeft() : offset;
        float right = rightOffsetValue == null ? viewPortHandler.offsetRight() : offset;
        float top = viewPortHandler.offsetTop();
        float bottom = viewPortHandler.offsetBottom();
        chart.setViewPortOffsets(left, top, right, bottom);
    }

    private void addGradient() {
        LinearGradient gradient = new LinearGradient(
                0f, 0, 0f, chart.getHeight(),
                colors,
                positions,
                Shader.TileMode.CLAMP
        );
        Paint paint = chart.getRenderer().getPaintRender();
        paint.setShader(gradient);
    }

    private void addVerticalGradient() {
        int[] colors = new int[]{ContextCompat.getColor(getContext(), R.color.transparent),
                ContextCompat.getColor(getContext(), R.color.gray)};
        LinearGradient gradient = new LinearGradient(
                0f, 0f, 0f, chart.getHeight(),
                colors,
                null,
                Shader.TileMode.CLAMP
        );
        Paint paint = chart.getRendererXAxis().getPaintGrid();
        paint.setShader(gradient);
        Paint leftAxisPaint = chart.getRendererLeftYAxis().getPaintAxisLine();
        leftAxisPaint.setShader(gradient);
        Paint rightAxisPaint = chart.getRendererRightYAxis().getPaintAxisLine();
        rightAxisPaint.setShader(gradient);
        chart.getXAxis().setGridLineWidth(2f);
    }

    private List<Entry> setYAxisDots() {
        List<Entry> yVals = new ArrayList<>();

        for (int i = 0; i < values.size(); i++) {
            yVals.add(new Entry(i, values.get(i)));
        }

        return yVals;
    }

}
