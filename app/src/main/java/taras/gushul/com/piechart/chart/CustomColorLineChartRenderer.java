package taras.gushul.com.piechart.chart;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.github.mikephil.charting.animation.ChartAnimator;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.renderer.LineChartRenderer;
import com.github.mikephil.charting.utils.Transformer;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created: Gushul Taras
 * E-mail: gushultaras125@gmail.com
 * Company: APPGRANULA LLC
 * Date: 12.09.2018
 */
public class CustomColorLineChartRenderer extends LineChartRenderer {

    private static final float RADIUS_MULTIPLIER = 2f;
    private Paint circlePaint = new Paint();
    private WeakReference<Bitmap> customBitmap;
    private Map<Integer, Bitmap> circleBitmaps;
    private Canvas customCanvas;

    public CustomColorLineChartRenderer(LineDataProvider chart, ChartAnimator animator, ViewPortHandler viewPortHandler) {
        super(chart, animator, viewPortHandler);
        circlePaint.setStyle(Paint.Style.FILL);
        circlePaint.setAntiAlias(true);
        circleBitmaps = new HashMap<>();
    }

    @Override
    public void drawData(Canvas c) {
        int width = (int) mViewPortHandler.getChartWidth();
        int height = (int) mViewPortHandler.getChartHeight();
        if (customBitmap == null
                || (customBitmap.get().getWidth() != width)
                || (customBitmap.get().getHeight() != height)) {
            if (width > 0 && height > 0) {
                customBitmap = new WeakReference<>(Bitmap.createBitmap(width, height, mBitmapConfig));
                customCanvas = new Canvas(customBitmap.get());
            } else {
                return;
            }
        }
        customBitmap.get().eraseColor(Color.TRANSPARENT);
        customCanvas.drawRect(0, 0, width, height, mRenderPaint);
        super.drawData(c);
    }

    /**
     * buffer for drawing the circles
     */
    private float[] mCirclesBuffer = new float[2];

    @Override
    protected void drawCircles(Canvas c) {

        mRenderPaint.setStyle(Paint.Style.FILL);

        float phaseY = mAnimator.getPhaseY();

        mCirclesBuffer[0] = 0;
        mCirclesBuffer[1] = 0;

        List<ILineDataSet> dataSets = mChart.getLineData().getDataSets();

        for (int i = 0; i < dataSets.size(); i++) {

            ILineDataSet dataSet = dataSets.get(i);

            if (!dataSet.isVisible() || !dataSet.isDrawCirclesEnabled() ||
                    dataSet.getEntryCount() == 0)
                continue;

            mCirclePaintInner.setColor(dataSet.getCircleHoleColor());

            Transformer trans = mChart.getTransformer(dataSet.getAxisDependency());

            mXBounds.set(mChart, dataSet);

            float circleRadius = dataSet.getCircleRadius();
            float circleHoleRadius = dataSet.getCircleHoleRadius();
            boolean drawCircleHole = dataSet.isDrawCircleHoleEnabled() &&
                    circleHoleRadius < circleRadius &&
                    circleHoleRadius > 0.f;

            int boundsRangeCount = mXBounds.range + mXBounds.min;

            for (int j = mXBounds.min; j <= boundsRangeCount; j++) {

                Entry e = dataSet.getEntryForIndex(j);

                if (e == null) break;

                mCirclesBuffer[0] = e.getX();
                mCirclesBuffer[1] = e.getY() * phaseY;

                trans.pointValuesToPixel(mCirclesBuffer);

                if (!mViewPortHandler.isInBoundsRight(mCirclesBuffer[0]))
                    break;

                if (!mViewPortHandler.isInBoundsLeft(mCirclesBuffer[0]) ||
                        !mViewPortHandler.isInBoundsY(mCirclesBuffer[1]))
                    continue;

                Bitmap circleBitmap = createCircleBitmap(dataSet, drawCircleHole);

                if (circleBitmap != null) {
                    c.drawBitmap(circleBitmap, mCirclesBuffer[0] - circleRadius * RADIUS_MULTIPLIER, mCirclesBuffer[1] - circleRadius * RADIUS_MULTIPLIER, null);
                }
            }
        }
    }

    private Bitmap createCircleBitmap(ILineDataSet set, boolean drawCircleHole) {

        int bitmapWidth = customBitmap.get().getWidth();
        int bitmapHeight = customBitmap.get().getHeight();

        int x = (int) mCirclesBuffer[0];
        int y = (int) mCirclesBuffer[1];

        if (customBitmap == null || customBitmap.get() == null
                || x < 0 || y < 0
                || x > bitmapWidth || y > bitmapHeight) {
            return null;
        }

        if (circleBitmaps.containsKey(y)) {
            return circleBitmaps.get(y);
        }

        float circleRadius = set.getCircleRadius();
        float circleHoleRadius = set.getCircleHoleRadius();

        Bitmap.Config conf = Bitmap.Config.ARGB_4444;
        Bitmap circleBitmap = Bitmap.createBitmap((int) (circleRadius * 2.1 * RADIUS_MULTIPLIER), (int) (circleRadius * 2.1 * RADIUS_MULTIPLIER), conf);

        Canvas canvas = new Canvas(circleBitmap);

        int pixel = customBitmap.get().getPixel(x, y);
        circlePaint.setColor(pixel);

        circlePaint.setShadowLayer(circleRadius, 0, 0, pixel);

        canvas.drawCircle(
                circleRadius * RADIUS_MULTIPLIER,
                circleRadius * RADIUS_MULTIPLIER,
                circleRadius,
                circlePaint);

        if (drawCircleHole) {
            canvas.drawCircle(
                    circleRadius,
                    circleRadius,
                    circleHoleRadius,
                    mCirclePaintInner);
        }

        circleBitmaps.put(y, circleBitmap);

        return circleBitmap;
    }

}
