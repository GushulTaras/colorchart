package taras.gushul.com.piechart;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import taras.gushul.com.piechart.chart.ColorChart;

public class MainActivity extends AppCompatActivity {

    private ColorChart colorChart;

    private List<String> dates;
    private List<Float> values;
    private int[] colors;
    private float[] positions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        colorChart = findViewById(R.id.colorChart);

        createFakeData();

        colorChart.setData(100f, 0f, 80f, 0f, values, dates, colors, positions);
    }

    private void createFakeData() {
        values = new ArrayList<>();
        values.add(100f);
        values.add(50f);
        values.add(60f);
        values.add(30f);
        values.add(10f);
        values.add(0f);
        values.add(55f);
        values.add(67f);
        values.add(80f);
        values.add(55f);
        values.add(21f);
        values.add(46f);

        dates = new ArrayList<>();
        dates.add("1.09");
        dates.add("2.09");
        dates.add("3.09");
        dates.add("4.09");
        dates.add("5.09");
        dates.add("6.09");
        dates.add("7.09");
        dates.add("8.09");
        dates.add("9.09");
        dates.add("10.09");
        dates.add("11.09");
        dates.add("12.09");

        colors = new int[5];
        colors[0] = Color.GREEN;
        colors[1] = Color.RED;
        colors[2] = Color.BLUE;
        colors[3] = Color.YELLOW;
        colors[4] = Color.MAGENTA;

        positions = new float[5];
        positions[0] = 0f;
        positions[1] = 30f;
        positions[2] = 60f;
        positions[3] = 80f;
        positions[4] = 100f;
    }

}
