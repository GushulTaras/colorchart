package taras.gushul.com.piechart.chart;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.List;

/**
 * Created: Gushul Taras
 * E-mail: gushultaras125@gmail.com
 * Company: APPGRANULA LLC
 * Date: 06.09.2018
 */
public class CustomColorValueFormatter implements IAxisValueFormatter {

    private List<String> dates;

    public CustomColorValueFormatter(List<String> dates) {
        this.dates = dates;
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        if (value == -1 || (value == 1 && dates.size() == 1)) {
            return "";
        }
        return dates.get((int) value);
    }
}
